# 关于本系统

本系统是由crm8000二次开发而生的企业信息化管理软件。

# PSI的开源协议

PSI的开源协议为GPL v3

# PSI原项目地址

1. PSI使用帮助：https://gitee.com/crm8000/PSI_Help

2. PSI移动端：https://gitee.com/crm8000/PSI_Mobile

# 技术支持

如有技术方面的问题，可以提出Issue一起讨论：https://gitee.com/crm8000/PSI/issues

# 本地开发环境
参见：<a href="https://gitee.com/crm8000/PSI/tree/master/doc/06%20%E6%9C%AC%E5%9C%B0%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83">本地开发环境</a>

# 中小企业生产ERP/信息化管理系统二次合作

Email: 83298094@qq.com

# 鸣谢：
CRM8000提供原生源码
